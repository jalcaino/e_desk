$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("resources/features/UnabHomePage.feature");
formatter.feature({
  "name": "Unab Homepage",
  "description": "Esta caracteristica verifica la funcionalidad de la home page de Unab",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Chequea los principales elementos de la homepage de Unab cuando es desplegada",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Yo ejecuto firefox",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefs_UnabHomepage.i_launch_Chrome_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Yo abro la homepage de UNAB",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefs_UnabHomepage.i_open_Unab_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Yo verifico que existe el campo usuario",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefs_UnabHomepage.i_verify_that_the_page_displays_usuario_text_box()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "si esta desplegado el campo clave",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs_UnabHomepage.i_verify_that_the_page_displays_clave_text_box()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "si esta desplegado el boton ingresar",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs_UnabHomepage.i_verify_that_the_page_displays_ingresar_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Ingreso usuario \"jalcaino\" en campo usuario",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs_UnabHomepage.insert_a_text_in_usuario_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Ingreso password \"1111111111\" en campo clave",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs_UnabHomepage.insert_a_text_in_clave_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Voy a consultar los datos del alumno \"22195644-3\" colocando su rut",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs_UnabHomepage.insert_a_text_in_rut_field(String)"
});
formatter.result({
  "status": "passed"
});
});