Feature: Unab Homepage
Esta caracteristica verifica la funcionalidad de la home page de Unab

Scenario: Chequea los principales elementos de la homepage de Unab cuando es desplegada
Given Yo ejecuto firefox
When Yo abro la homepage de UNAB
Then Yo verifico que existe el campo usuario
And si esta desplegado el campo clave
And si esta desplegado el boton ingresar
And Ingreso usuario "jalcaino" en campo usuario
And Ingreso password "1111111111" en campo clave
And Voy a consultar los datos del alumno "22195644-3" colocando su rut