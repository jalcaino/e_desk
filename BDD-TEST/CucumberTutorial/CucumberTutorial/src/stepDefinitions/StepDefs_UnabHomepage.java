package stepDefinitions;

import cucumber.api.java.en.*;
import seleniumPages.Page_UnabHomepage;


public class StepDefs_UnabHomepage {
	
	Page_UnabHomepage unabHomePage = new Page_UnabHomepage();
	
	@Given("^Yo ejecuto firefox$")
	public void i_launch_Chrome_browser() {
		unabHomePage.launchBrowser();
		System.out.println("Ejecutar Firefox");
	    
	}

	@When("^Yo abro la homepage de UNAB$")
	public void i_open_Unab_Homepage() {
		unabHomePage.openUnabURL();
		System.out.println("Abro Unab");
	    
	}

	@Then("^Yo verifico que existe el campo usuario$")
	public void i_verify_that_the_page_displays_usuario_text_box() {
		unabHomePage.checkUsuarioBoxIsDisplayed();
		System.out.println("verifico campo usuario");
	}

	@Then("^si esta desplegado el campo clave$")
	public void i_verify_that_the_page_displays_clave_text_box() {
		unabHomePage.checkClaveBoxIsDisplayed();
		System.out.println("verifico campo clave");
	 
	}

	@Then("^si esta desplegado el boton ingresar$")
	public void i_verify_that_the_page_displays_ingresar_button() {
		unabHomePage.checkIngresarButtonIsDisplayed();
		System.out.println("verifica boton ingresar");
	   
	}
	@Then("^Ingreso usuario \"([^\"]*)\" en campo usuario$")
	public void insert_a_text_in_usuario_field(String arg1) {
	   unabHomePage.ingresousuario(arg1);
	   System.out.println("ingresando usuario:"+arg1);
	}

	@Then("^Ingreso password \"([^\"]*)\" en campo clave$")
	public void insert_a_text_in_clave_field(String arg2) {
	   unabHomePage.ingresopassword(arg2);
	   System.out.println("ingresando clave:"+arg2);
	}

	
	@Then("^Voy a consultar los datos del alumno \"([^\"]*)\" colocando su rut$")
	public void insert_a_text_in_rut_field(String arg3){
	   unabHomePage.ingresorut(arg3);
	   System.out.println("ingresando rut a consultar:"+arg3);
	}

	
}
