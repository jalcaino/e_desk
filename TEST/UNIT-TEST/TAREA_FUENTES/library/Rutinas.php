<?php

class Rutinas
{

    public function validaRut($trut)
    {
    
        $porciones = explode("-",$trut);
        if(count($porciones)<2)
            return 0;
        
        $dvt = substr($trut, strlen($trut) - 1, strlen($trut));
        $rutt = substr($trut, 0, strlen($trut) - 1);
        $rut = (($rutt) + 0);
        $pa = $rut;
        $c = 2;
        $sum = 0;
        
        while ($rut > 0)
        {
        
        	$a1 = $rut % 10;
        
        	$rut = floor($rut / 10);
        	$sum = $sum + ($a1 * $c);
        	$c = $c + 1;

        	if ($c == 8)
        	{
        		$c = 2;
        	}
        }
        
        $di = $sum % 11;
        $digi = 11 - $di;
        $digi1 = ((string )($digi));

        if (($digi1 == '10'))
        {
        	$digi1 = 'K';
        }
        
        if (($digi1 == '11'))
        {
        	$digi1 = '0';
        }
        
        if (($dvt == $digi1))
        	return 1;
        else
            return 0;
        	 
    }
    
    public function validaEmail($email)
    {
    
        $porciones = explode("@",$email);
        if(count($porciones)<2)
        	return 0;
        
        
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
            return 0;
        else 
            return 1;    
        
    }
               

}
?>