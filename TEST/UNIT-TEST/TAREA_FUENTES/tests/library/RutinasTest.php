<?php
require_once 'library/Rutinas.php';

require_once 'PHPUnit/Framework/TestCase.php';

/**
 * Rutinas test case.
 */
class RutinasTest extends PHPUnit_Framework_TestCase
{

    /**
     *
     * @var Rutinas
     */
    private $Rutinas;

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp ()
    {
        parent::setUp();
        
        // TODO Auto-generated RutinasTest::setUp()
        
        $this->Rutinas = new Rutinas(/* parameters */);
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown ()
    {
        // TODO Auto-generated RutinasTest::tearDown()
        $this->Rutinas = null;
        
        parent::tearDown();
    }

    /**
     * Constructs the test case.
     */
    public function __construct ()
    {
        // TODO Auto-generated constructor
    }

    /**
     * Tests Rutinas->validaRut()
     */
    public function testValidaRutNum ()
    {
        
        $result = $this->Rutinas->validaRut("1");
        $this->assertEquals(0,$result);
    
    }

    public function testValidaRutLetra ()
    {
    
    	$result = $this->Rutinas->validaRut("aa");
    	$this->assertEquals(0,$result);
    
    }

    
    public function testValidaRutSinGuion ()
    {
    
    	$result = $this->Rutinas->validaRut("11111111@1");
    	$this->assertEquals(0,$result);
    
    }

    public function testValidaRutDv ()
    {
    
    	$result = $this->Rutinas->validaRut("11111111-1");
    	$this->assertEquals(1,$result);
    
    }
    
   
   
   /*********************/

    public function testValidaEmailNum ()
    {
    
    	$result = $this->Rutinas->validaEmail("1");
    	$this->assertEquals(0,$result);
    
    }
    

    public function testValidaEmailLetra ()
    {
    
    	$result = $this->Rutinas->validaEmail("a");
    	$this->assertEquals(0,$result);
    
    }
    

    public function testValidaEmailConArroba ()
    {
    
    	$result = $this->Rutinas->validaEmail("cherrera2019@gmail.com");
    	$this->assertEquals(1,$result);
    
    }

    public function testValidaEmailSinArroba ()
    {
    
    	$result = $this->Rutinas->validaEmail("cherrera2019-gmail.com");
    	$this->assertEquals(0,$result);
    
    }
    
}